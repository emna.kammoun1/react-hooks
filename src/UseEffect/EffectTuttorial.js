import React, { useEffect, useState } from "react";
//affichage chaque reload 
import axios from 'axios'
const EffectTuttorial = () => {
    const [data ,setData]=useState("")
    const [count ,setCount]=useState(0)

    useEffect(() => {
        axios
          .get("https://jsonplaceholder.typicode.com/comments")
          .then((response) => {
            setData(response.data[0].email);
            console.log("API WAS CALLED");
          });
      }, [count]);

//fait loader chaque incrementation en fonction de count


  return (
    <div>
      Hello {data}
      <h1>{count}</h1>
      <button onClick={() => setCount(count +1)}>++</button>
    </div>
  )
}

export default EffectTuttorial
