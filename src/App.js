import CallBackTutorial from './UseCallback/CallbackTutorial';
import EffectTuttorial from './UseEffect/EffectTuttorial';
import ImperativeHandle from './UseImperativeHandle/ImperativeHandle';
import LayoutEffectTutorial from './UseLayoutEffect/LayoutEffectTutorial';
import MemoTurorial from './UseMemo/MemoTurorial';
import ReducerTutorial from './UseReducer/ReducerTutorial';
import RefTutorial from './UseRef/RefTutorial';
import StateTutorial from './UseState/StateTutorial';
import ContextTutorial from './UserContext/ContextTutorial';

function App() {
  return (
    <div>
      <StateTutorial/>
    <ReducerTutorial/>
    <EffectTuttorial />
    <RefTutorial />
    <LayoutEffectTutorial />
    <ImperativeHandle />
    <ContextTutorial />
    <MemoTurorial />
    <CallBackTutorial />
    </div>
  );
}

export default App;
