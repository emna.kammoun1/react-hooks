import React,{useReducer} from 'react'
//si ilya deux action dans onchange on utilise useRedcer
const reducer = (state ,action) => {
    switch(action.type) {
        case "INCREMENT" :
            return {count : state.count +1 ,showText: state.showText}
        case "toggleShowText" :
            return {count : state.count  ,showText: !state.showText}
        default :
            return state
            
    }
}
    



const ReducerTutorial = () => {
    const [state , dispatch] = useReducer(reducer ,{count :0 ,showText : false})

  return (
    <div>
        <h1>{state.count}</h1>
        <button
        onClick={() => {
          dispatch({ type: "INCREMENT" });
          dispatch({ type: "toggleShowText" });
        }}
      >
        Click Here
      </button>
      {state.showText && <a>This is a thext</a>}
    </div>
  )
}

export default ReducerTutorial
