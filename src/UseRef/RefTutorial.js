import React , {useRef, useState} from 'react'

const RefTutorial = () => {
    const inputRef = useRef(null)
    const [value,setValue] = useState("")
    const onClick = () => {
    inputRef.current.focus()
    //une fois on clique il ya focus sur input
    setValue(inputRef.current.value)
    inputRef.current.value =""

    }
  return (
    <div>
        <h1>{value}</h1>
        <input type="text" placeholder='EX .......' ref={inputRef}/>
        <button onClick={onClick}>Change Name</button>
      
    </div>
  )
}

export default RefTutorial
