import React ,{useContext} from 'react'
import { AppContext } from './ContextTutorial'
function User() {
    const {username} = useContext(AppContext)
    //acceder à la vaariable username du context trouve dans context
  return (
    <div>
        <h1> User : {username} </h1>

    </div>
  )
}

export default User
