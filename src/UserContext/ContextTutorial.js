import React, { useState ,createContext } from 'react'
import Login from './Login'
import User from './User'
// permet de simplifier le code et de rendre le partage de données plus efficace.
export const AppContext = createContext(null)

function ContextTutorial() {
    const [username ,setUsername] = useState("")
  return (
    <AppContext.Provider value={{ username , setUsername}}>
        <Login/>
        <User />
    </AppContext.Provider>
  )
}

export default ContextTutorial
