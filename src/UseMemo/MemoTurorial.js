import React , {useEffect ,useState ,useMemo}from 'react'
import axios from 'axios';

function MemoTurorial() {
    const [data,setData] = useState(null)
    const [toggle ,setToggle] = useState(false)
    useEffect(() => {
        axios
          .get("https://jsonplaceholder.typicode.com/comments")
          .then((response) => {
            setData(response.data);
           // console.log(data)
          });
      }, []);
      const findLongestName = (comments) => {
        if (!comments) return null
        let longestName =""
        for (let i =0; i< comments.length ; i++) {
            let currentName = comments[i].name 
            if (currentName.length > longestName.length){
                longestName = currentName
            }
        }
        console.log("this was computed")
        //console.log(longestName)

        return  longestName
      }
      const getLongestName = useMemo (()=> findLongestName(data),[toggle])
            //call function in useMemo au lien de  findLongestName(data)
            //pas de console.log chaque clique sur toggle

  return (
    <div className='App'>
        <div>
            {getLongestName}
        </div>
        <button onClick={() => {setToggle(!toggle)}}>{""} Toggle</button>
        {toggle &&<h1> toggle</h1>}
        
      
    </div>
  )
}

export default MemoTurorial
