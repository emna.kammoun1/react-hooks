import React,{useState} from 'react'
//change the value of variable
const StateTutorial = () => {
    //let counter=0
const [counter,setCounter] = useState(0)
const [inputValue,setInputValue] = useState ("")
    const increment = () => {
        setCounter(counter+1)
        //counter=counter+1

    }
    let onChange =(event) => {
        const newValue =event.target.value
        setInputValue(newValue)
    }
  return (
    <div>
      {counter} <button onClick={increment}>+</button>
      <div>

      <input placeholder='enter Something .....' onChange={onChange}/>
      {inputValue}
    </div>   
     </div>

  )
}

export default StateTutorial
